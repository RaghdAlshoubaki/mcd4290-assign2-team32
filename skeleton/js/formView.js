"use strict";

//Location tracking
function Autoaddress() 
{
    navigator.geolocation.getCurrentPosition(success)
    function success(pos) 
    {
        var crd = pos.coords;
        console.log('Your current position is:');
        console.log(`Latitude : ${crd.latitude}`);
        console.log(`Longitude: ${crd.longitude}`);
        console.log(`More or less ${crd.accuracy} meters.`);
        var latitude = crd.latitude;
	    var longitude = crd.longitude;
	    var apikey = '80811cb1600744dbb405a3ca403a5f61';
        var api_url = 'https://api.opencagedata.com/geocode/v1/json'
        var request_url = api_url
        + '?'
        + 'key=' + apikey
        + '&q=' + encodeURIComponent(latitude + ',' + longitude)
        + '&pretty=1'
        + '&no_annotations=1'; 
        // see full list of required and optional parameters:
        // https://opencagedata.com/api#forward
        var request = new XMLHttpRequest();
        request.open('GET', request_url, true);
        request.onload = function() 
        {
        // see full list of possible response codes:
        // https://opencagedata.com/api#codes
            if (request.status == 200)
            {  // Success!
                var data = JSON.parse(request.responseText);
                var address=data.results[0].formatted
                var commasposition=address.indexOf(",")
                address=address.slice(0,commasposition)
                document.getElementById("address").value=address;
            } else if (request.status <= 500)
            { 
               // We reached our target server, but it returned an error
                console.log("unable to geocode! Response code: " + request.status);
                var data = JSON.parse(request.responseText);
                console.log(data.status.message);
            } else 
            {
                console.log("server error");
            }
        };
        request.onerror = function() 
        {
              // There was a connection error of some sort
            console.log("unable to connect to server");        
        };
        request.send();  // make the request
    }
}


function Save()
{
  let address=document.getElementById("address").value
  let useAddress=document.getElementById("useAddress").checked
  let roomNumber=document.getElementById("roomNumber").value
  let lightsOn=document.getElementById("lights").checked
  let heatingCoolingOn=document.getElementById("heatingCooling").checked
  let seatsUsed=document.getElementById("seatsUsed").value
  let seatsUsedNum=Number(seatsUsed)
  let seatsTotal=document.getElementById("seatsTotal").value
  let seatsTotalNum=Number(seatsTotal)
  let message=""
  let checkSave= true 
  
  document.getElementById("message").innerHTML=""
    
    if(typeof(address)!=="string")
    {  
    message+="Please enter a valid [Building Address]!" +"<br>"
    checkSave= false
    }
    if(address==="")
    {
    message+="[Building Address] can't be empty!"+"<br>"    
    checkSave= false
    }
    if(roomNumber==="")
    {
    message+="[roomNumber] can't be empty!"+"<br>"    
    checkSave= false
    }
   
    if(isNaN(Number(roomNumber))!==false)
    {  
    message+="Please enter a valid [Room Number]!"+"<br>" 
    checkSave= false
    }
    if(seatsUsed==="")
    {
    message+="[seatsUsed] can't be empty!"+"<br>"    
    checkSave= false
    }
    
    else if(isNaN(seatsUsedNum)==true || Number.isInteger(seatsUsedNum)==false ||seatsUsedNum<0)
    {
    message+="please fill in [Number of sests in use] with NUMBER and make sure it's POSITIVE INTEGER!"+"<br>"    
    checkSave= false
    }
    if(seatsTotal==="")
    {
    message+="[seatsTotal] can't be empty!"+"<br>"    
    checkSave= false
    }
    else if(isNaN(seatsTotalNum)==true || Number.isInteger(seatsTotalNum)==false ||seatsTotalNum<0)
    {
    message+="please fill in [Number of available sests] with NUMBER and make sure it's POSITIVE INTEGER!"+"<br>"    
    checkSave= false
    }
       
    if (seatsUsedNum > seatsTotalNum)
    {
    message+="[Number of sests in use] should not greater than [Number of available sests]!"+"<br>"    
    checkSave= false
    }
        
    if (checkSave == true)
    {
       var temp_roomusage=new RoomUsage(roomNumber,address,lightsOn,heatingCoolingOn,seatsUsedNum,seatsTotalNum)
       temp_roomusage.Date()
       
            var RoomUsageListObj=FromLocalStorageToPDO()
            //console.log(RoomUsageListObj)
            roomusagelist.InitializeFromPDO(RoomUsageListObj)
            var list=roomusagelist.roomList
            list.push(temp_roomusage)
            StoreRoomUsageList(roomusagelist)
            alert("roomusage class created")
    }
 
    document.getElementById("message").innerHTML=message
    
}

  let switch1=[lights,heatingCooling]   //this represent 
   
  
function Clear()
{  
  let address=document.getElementById("address").value
  let useAddress=document.getElementById("useAddress")
  let roomNumber=document.getElementById("roomNumber").value
  let lightsOn=document.getElementById("lights").checked
  let heatingCoolingOn=document.getElementById("heatingCooling").checked
  let seatsUsed=document.getElementById("seatsUsed").value
  let seatsUsedNum=Number(seatsUsed)
  let seatsTotal=document.getElementById("seatsTotal").value
  let seatsTotalNum=Number(seatsTotal)
  let message=""

    document.getElementById("address").value=''
    document.getElementById("roomNumber").value=''
    for(var i=0;i<switch1.length;i++)
{
    switch1[i].parentNode.MaterialSwitch.on()//switch back toggle by using mdl documentation
   
}
    useAddress.parentNode.MaterialCheckbox.uncheck("")
    document.getElementById("seatsUsed").value=''
    document.getElementById("seatsTotal").value=''
    document.getElementById("address").parentNode.MaterialTextfield.change("")
    document.getElementById("roomNumber").parentNode.MaterialTextfield.change("")
    document.getElementById("seatsUsed").parentNode.MaterialTextfield.change("")
    document.getElementById("seatsTotal").parentNode.MaterialTextfield.change("")
    document.getElementById("message").innerHTML=""
    alert("cleared")
    
}