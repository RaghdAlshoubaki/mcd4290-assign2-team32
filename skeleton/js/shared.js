"use strict";
//F1

const key="ENG1003_RoomUseList"

class RoomUsage
{    constructor(roomNumber,address,lightsOn,heatingCoolingOn,seatsUsed,seatsTotal,timeChecked,Occupancy)
    {
        //Private Attributes
        this._roomNumber=roomNumber
        this._address=address
        this._lightsOn=lightsOn
        this._heatingCoolingOn=heatingCoolingOn
        this._seatsUsed=seatsUsed
        this._seatsTotal=seatsTotal
<<<<<<< Updated upstream
        this._timeChecked=timeChecked
        this._Occupancy=Occupancy
    }     //the datatype and restrictions is gonna be written as [function Save] in formview.js
 
    get roomNumber()
    {
        return this._roomNumber
    }
    set roomNumber(x)
    {
        this._roomNumber=x
    }
    get address()
    {
        return this._address
    }
    set address(x)
    {
        this._address=x
    }
    get lightsOn()
    {
        return this._lightsOn
    }
    set lightsOn(x)
    {
        this._lightsOn=x
    }
       get heatingCoolingOn()
    {
        return this._heatingCoolingOn
    }
    set heatingCoolingOn(x)
    {
        this._heatingCoolingOn=x
    }
    get seatsUsed()
    {
        return this._seatsUsed
    }
    set seatsUsed(x)
    {
        this._seatsUsed=x
    }
    get seatsTotal()
    {
        return this._seatsTotal
    }
    set seatsTotal(x)
    {
        this._seatsTotal=x
    }
    get timeChecked()
    {
        return this._timeChecked
    }
    set timeChecked(x)
    {
        this._timeChecked=x
    }
    CalcuOccupancy()
    {
        var Occu=this._seatsUsed/this._seatsTotal
        if(isNaN(Occu)==true)
            {
                Occu=0
            }
        Occu=Occu.toFixed(2)
        return Occu
    }
    get Occupancy()
    {
        return this._Occupancy
    }
    set Occupancy(x)
    {
        this._Occupancy=x
    }
    get timeChecked()
    {
        return this._timeChecked
    }
    set timeChecked(x)
    {
        this._timeChecked=x
    }
    Date()
    {
        var time=new Date()
        this._timeChecked=time
    }
    isWasteful()
    {
        return this._seatsUsed==0&&(this._heatingCoolingOn||this._lightsOn)
    }
    InitializeRoomusagePDO(RoomusageObj)
    {
        this._roomNumber=RoomusageObj._roomNumber
        this._address=RoomusageObj._address
        this._lightsOn=RoomusageObj._lightsOn
        this._heatingCoolingOn=RoomusageObj._heatingCoolingOn
        this._seatsUsed=RoomusageObj._seatsUsed
        this._seatsTotal=RoomusageObj._seatsTotal
        this._timeChecked=new Date(RoomusageObj._timeChecked) 
        var Occu=RoomusageObj._seatsUsed/RoomusageObj._seatsTotal
        if(isNaN(Occu)==true)
            {
                Occu=0
            }
        this._Occupancy=Occu.toFixed(3)
    }
} 

<<<<<<< HEAD
=======
        let time= new Date()
        this._timeChecked= time
    }
    set roomNumber(newNumber)
    {
    if(typeof(newNumber) === "string")
    {
        if newNumber !== "" 
        {
          this._roomNumber=newNumber
        }
    }
    }
   set address(newAddress)
    {
    if(typeof(newAddress) === "string")
    {
        if newAddress !== "" 
        {
          this._address=newAddress
    }
    }
    }
    
    set lightsOn(newLights)
    {
        if(typeof(newLights)==="boolean")
            {
                this._lightsOn=newLights
            }
    }
    
    set heatingCoolingOn(newHeating) 
    {
    if (typeof(newHeating)==="boolean")
        {
            this._heatingCoolingOn= newHeating
        }
    }
    
    set (seatsTotal(newSeats) & seatsUsed(newSeats)) 
    {
    if(seatsUsed)>0 && (seatsTotal)>0 
    {
        if(seatsTotal >= seatsUsed)
        {
            this._seatsTotal=newSeats
            this._seatsUsed=newSeats
        }
        }
    }
}
>>>>>>> Stashed changes
=======

>>>>>>> master
//F2    
class RoomUsageList 
{
  constructor(roomList) {
    this._roomList = roomList;
  }
  get roomList() {
    return this._roomList;
  }
  set roomList(x) {
    this._roomList = x;
  }
 
  InitializeFromPDO(RoomUsageListObj)
    {
        this._roomList=[]
        for(var index=0;index<RoomUsageListObj._roomList.length;index++)
            {
                var roomusage=new RoomUsage()
                roomusage.InitializeRoomusagePDO(RoomUsageListObj._roomList[index])
                AddRoomUsage(roomusage)
            }
    }
  
  aggregateBy()
    {
        var bucket=[]
        bucket.timeBucket={}
        bucket.buildingBucket={}
        for(var i=0;i<this._roomList.length;i++)
            {
                var timeObj=new Date(this._roomList[i].timeChecked)
                var timekey=timeObj.getHours()
                var addresskey=this._roomList[i].address
                if(bucket.timeBucket.hasOwnProperty(timekey)==false)
                   { 
                       Object.defineProperty(bucket.timeBucket,timekey, {value :[this._roomList[i]],
                           writable : true,
                           enumerable : true,
                           configurable : true});
                   }
                else
                    {
                        bucket.timeBucket[timekey].push(this._roomList[i])
                    }
                if(bucket.buildingBucket.hasOwnProperty(addresskey)==false)
                   { 
                       Object.defineProperty(bucket.buildingBucket,addresskey, {value :[this._roomList[i]],
                           writable : true,
                           enumerable : true,
                           configurable : true});
                   }
                else
                    {
                        bucket.buildingBucket[addresskey].push(this._roomList[i])
                    }
            }
        return bucket
    }
     WorstOccupancy(timeBucket)
    {
        var timeBucket_1stHr=Object.keys(timeBucket)[0]
        var WorstOccuBucket={}
        for(var hours=timeBucket_1stHr;hours<=18;hours++)//gp through each hours from 9am to 6pm
            {
                var hoursBucket=timeBucket[hours]
                var Worst5rOccu
                var OccuBucket=[]
                if(hoursBucket==undefined)
                    {
                    }
                else if(hoursBucket.length<5)//to check if the OccuBucket's length is more than or equal to 5 
                { 
                }
                else //to check if the OccuBucket's length is more than or equal to 5 
                { 
                    for(var i=0;i<hoursBucket.length;i++)
                        {
                          OccuBucket.push(hoursBucket[i].Occupancy)//i refers to pos of each element in hoursBucket
                        }
                
                    var SortBucket=OccuBucket.sort(function(a, b){return a - b})
                    Worst5rOccu=SortBucket.splice(5,SortBucket.length)
                    var Worst5roomAthours=[]//prepare an empty array for 5 worstoccu room whose each element is in form of roomusage
                    for(var i=0;i<timeBucket[hours].length;i++)
                        {
                          var timeBucketAthoursOccu=timeBucket[hours][i].Occupancy
                          for(var p=0;p<5;p++)//p is the default length of Worst5rOccu
                           {
                              if(timeBucketAthoursOccu==Worst5rOccu[p])
                                {
                                   Worst5roomAthours[p]=timeBucket[hours][i]//push worst 5 rooms into  Worst5roomAthours on descending order
                                   Object.defineProperty(WorstOccuBucket,hours, {value :Worst5roomAthours,
                                   writable : true,
                                   enumerable : true,
                                   configurable : true})
                                }
                           }
                        }
                }
            }
        return WorstOccuBucket
    }
    Statistics(buildingBucket)
    {
        var AllBuildingStatistics={}
        for(var prop in buildingBucket)
            {
                var BuildingStatistics=StatisticsForBuilding(prop,buildingBucket)
                Object.defineProperty(AllBuildingStatistics,prop, {value :BuildingStatistics,
                                   writable : true,
                                   enumerable : true,
                                   configurable : true})
            }
        return AllBuildingStatistics
    }

}

var roomusagelist=new RoomUsageList()
function FromLocalStorageToPDO()
  {
    var RoomUsageListObj
    if(localStorage.getItem(key)!=null)
       {
           RoomUsageListObj=JSON.parse(localStorage.getItem(key))
       }
    else
       {
           var NewRoomUsageClass=new RoomUsageList([])
           var JSONRoomUsageClass=JSON.stringify(NewRoomUsageClass)
           localStorage.setItem(key,JSONRoomUsageClass)
           RoomUsageListObj=JSON.parse(localStorage.getItem(key))
       }
      return RoomUsageListObj
  }

function AddRoomUsage(roomusage)
{
    var Newroomlist=roomusagelist.roomList
    Newroomlist.push(roomusage)
    roomusagelist.roomList=Newroomlist
}
function StoreRoomUsageList(roomusagelist)
{
    var JSONRoomUsageList=JSON.stringify(roomusagelist)//extract roomusagelist in form of object,the datatype can stored in localstorage and used persistently
    localStorage.setItem(key,JSONRoomUsageList)
}

//F6
function loadlistfromlocalstorage(key){//key is a parameter to get access to localstorage with that key
    var LocalStorageRoomusagelist=new RoomUsageList([])
    if(localStorage.getItem(key)!=null)
    {
        var JSONroomusagelist=JSON.parse(localStorage.getItem(key))
      for(var index=0;index<JSONroomusagelist._roomList.length;index++)
       {
        var roomusage=new RoomUsage()
        roomusage.InitializeRoomusagePDO(JSONroomusagelist._roomList[index])
        var Newroomlist=LocalStorageRoomusagelist.roomList
        Newroomlist.push(roomusage)
        LocalStorageRoomusagelist.roomList=Newroomlist
       }
        return LocalStorageRoomusagelist
    }else
    {
        localStorage.setItem(key,JSON.stringify(roomusagelist))
    } 
}

function PercentageConvertor(Numerator,Denominator)
{
    var Result=Numerator/Denominator//toFixed method concert number into a string,hence this method is used after any other calculations are done
    if(Result==NaN)
        {
            Result=0+"%"
        }
    var ResultMultiply100=Result*100//multiply result by 100 first to valid this function
    var ResultInPercentage=ResultMultiply100.toFixed(3)
    return String(ResultInPercentage)+"%"
}

function StatisticsForBuilding(prop,buildingBucket)
{
                var WastefulObservations=0
                var SeatsUsedTotal=0
                var SeatsInTotal=0
                var lightsOnTotal=0
                var heatingCoolingOnTotal=0
                var CertainOBuilding=buildingBucket[prop]
                var ObservationsTotal=CertainOBuilding.length
                for(var index=0;index<CertainOBuilding.length;index++)
                    {
                        var CertainObservation=CertainOBuilding[index]
                        
                        if(CertainObservation.isWasteful()==true)
                            {
                                WastefulObservations++
                            }
                        SeatsUsedTotal+=CertainObservation.seatsUsed
                        SeatsInTotal+=CertainObservation.seatsTotal
                        if(CertainObservation.lightsOn==true)
                            {
                                lightsOnTotal++
                            }
                        if(CertainObservation.heatingCoolingOn==true)
                            {
                                heatingCoolingOnTotal++
                            }
                    }
                var SeatsUtilization= PercentageConvertor(SeatsUsedTotal,SeatsInTotal)
                var lightsUtilization= PercentageConvertor(lightsOnTotal,ObservationsTotal)
                var heatingCoolingUtiliztion=PercentageConvertor(heatingCoolingOnTotal,ObservationsTotal)
                var StatisticsForBuilding=[]
                StatisticsForBuilding.ObservationsTotal=ObservationsTotal
                StatisticsForBuilding.WastefulObservations=WastefulObservations
                StatisticsForBuilding.SeatsUtilization=SeatsUtilization
                StatisticsForBuilding.lightsUtilization=lightsUtilization
                StatisticsForBuilding.heatingCoolingUtiliztion=heatingCoolingUtiliztion
                return StatisticsForBuilding
}