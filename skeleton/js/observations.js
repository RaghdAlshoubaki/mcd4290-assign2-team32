"use strict";

 //F7:Display the list of the recorded observations in reverse order 
let newArray;
function retrieveArrayInLocalStorage(key)
{
    if (typeof(LocalStorageRoomusagelist)!= undefined) 
        {
            newArray =JSON.parse(localStorage.getItem(key))
            if (newArray !== null)
            {
                if (newArray[newArray.length-1]===null)
                {
                    newArray.pop();
                }
            }
        } else 
        {
            alert('Error: local storage is undefined')
        }
        return newArray;
}

function DisplayObservationPage(usedArray,a)
{
    let calendar= new Date(usedArray._roomList[a]._timeChecked)
    let month=calendar.getMonth();
    if (month===0)
    {
        month = 'Jan.'
    }else if(month===1)
    {
        month='Feb.'
    }else if(month===2)
    {
        month='Mar.'
    }else if(month===3)
    {
        month='Apr.'
    }else if(month===4)
    {
        month='May.'
    }else if(month===5)
    {
        month='Jun.'
    }else if(month===6)
    {
        month='Jul.'
    }else if(month===7)
    {
        month='Aug.'
    }else if(month===8)
    {
        month='Sep.'
    }else if(month===9)
    {
        month='Oct.'
    }else if(month===10)
    {
        month='Nov.'
    }else
    {
        month='Dec.'
    } 
    let lights = usedArray._roomList[a]._lightsOn;
    if (lights===true)
    {
        lights='On';
    }else
    {
        lights='Off';
    }
    let heatingCooling = usedArray._roomList[a]._heatingCoolingOn;
    if (heatingCooling===true)
    {
        heatingCooling='On';
    }else
    {
        heatingCooling='Off';
    }
    var commasposition=usedArray._roomList[a]._address.indexOf(",")
    var address=usedArray._roomList[a]._address.slice(0,commasposition)
    document.getElementById("content").innerHTML += 
        `<div class="mdl-cell mdl-cell--4-col" id = ${a}>
             <table class="observation-table mdl-data-table mdl-js-data-table mdl-shadow--2dp">
                 <thead>
                     <tr>
                        <th class="mdl-data-table__cell--non-numeric"> 
                            <h4 class="date"> ${calendar.getDate()} ${month}</h4>
                                <h4>   ${address}<br />
                                        Room: ${usedArray._roomList[a]._roomNumber}
                                </h4>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="mdl-data-table__cell--non-numeric">
                                    Time: ${calendar.getHours()}:${calendar.getMinutes()}:${calendar.getSeconds()}<br />
                                    Lights: ${lights}<br />
                                    Heating/cooling: ${heatingCooling}<br />
                                    Seat usage: ${usedArray._roomList[a]._seatsUsed}/ ${usedArray._roomList[a]._seatsTotal}<br/>
                                    <button class="mdl-button mdl-js-button mdl-button--icon" onclick="myDeleteFunc(${0},'${calendar}')">
                                    <i class="material-icons">delete</i>
                                    </button>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>`
} 

function displayRoom(usedArray)
{
    if (usedArray._roomList.length !== undefined)
    {
        for (let a=usedArray._roomList.length-1; a>=0; a--)
        {
            DisplayObservationPage(usedArray,a)
        }
    }
}
    if (retrieveArrayInLocalStorage(key)!== null)
    {
        displayRoom(newArray);
    }
   
//F8: Deleting observations
function myDeleteFunc(index){
   document.getElementById(`cell${index}`).remove();
    newArray._roomList.splice(index,1)
    localStorage.setItem(key,JSON.stringify(newArray))
    window.location.reload()
}
   /* if (document.getElementById('searchField').value===""){
        retrieveArrayInLocalStorage(key);
        for(let k=0; k<newArray.length;k++){
            if (index===k){
                newArray.splice(i,1);
                break;
            }
        }
        localStorage.setItem(key, JSON.stringify(newArray))
        window.location.reload();
    }else{
        for (let l=0; l<newArray.length;l++){
            let deleteCalendar=newArray[l]._timeChecked
            if (string(new Date(deleteCalendar))===DateObj){
                newArray[l];
                newArray.splice(l,1);
                break;
            }
        }
        localStorage.setItem(key, JSON.stringify(newArray))
        window.location.reload();
    }  
}
*/
    // myroom.splice(a,a+1)
   //a is the correpounding indice of roomlist array in block that will be deleted(datatype of a is number)
  /*let Observation_At_i=document.getElementById("content").getElementsByClassName("mdl-cell mdl-cell--4-col")[i]
   Observation_At_i.style.display="none"
   roomusagelist.getroomList().splice(i, i+1)
   storeroomusagelist(roomusagelist)
}
*/

//F9: Searching the observations
let input= document.querySelector('input');
let searchField = document.getElementById('searchField')
    
function searchObservation(newArray)
{
    var Searchterm=document.getElementById("searchField").value
    var filter=Searchterm.toLowerCase()
    var Observation=newArray._roomList
    document.getElementById('SearchTermInfo').innerText="Search for "+'"'+Searchterm+'"'
    for(var index=Observation.length-1;index>=0;index--)
        {
            var RoomusageOfObservation=Observation[index]
            var roomNumber=RoomusageOfObservation._roomNumber
            var roomNumberLowercsae=roomNumber.toLowerCase()
            var address=RoomusageOfObservation._address
            var addressLowercsae=address.toLowerCase()
            var ObservationElement=document.getElementById(index)
            
            if(roomNumberLowercsae.indexOf(filter)!=-1)
                {
                    ObservationElement.style.display=""
                }else if(addressLowercsae.indexOf(filter)!=-1)
                {
                    ObservationElement.style.display=""
                }else
                {
                    ObservationElement.style.display="none"
                }
        }
}
