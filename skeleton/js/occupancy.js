"use strict";

// F11: worst occupancy by hour
var content=document.getElementById("content")
var roomusagelist=loadlistfromlocalstorage(key)
var Bukcet=roomusagelist.aggregateBy()
var timeBucket=Bukcet["timeBucket"]
var WorstOccuBucket=roomusagelist.WorstOccupancy(timeBucket)
var prop
for(prop in WorstOccuBucket)
{
    var node = document.createElement("div");
    var propNum=Number(prop)
    node.setAttribute("class", "mdl-cell mdl-cell--4-col");
    var table=document.createElement("table")
    table.setAttribute("class","mdl-data-table mdl-js-data-table mdl-shadow--2dp")
    table.setAttribute("id","WorstOccupancyByHour")
    var thead=document.createElement("thead")
    var tr=document.createElement("tr")
    var th=document.createElement("th")
    th.setAttribute("class","mdl-data-table__cell--non-numeric")
    var h5=document.createElement("h5")
    h5.innerText="Worst occupancy for "+prop+" o'clock"
    th.appendChild(h5)
    tr.appendChild(th)
    thead.appendChild(tr)
    table.appendChild(thead)
    node.appendChild(table)
    content.appendChild(node)
    for(var index=0;index<WorstOccuBucket[prop].length;index++)
    {
        var tbody=document.createElement("tbody")
        var tr=document.createElement("tr")
        var td=document.createElement("td")
        td.setAttribute("class","mdl-data-table__cell--non-numeric")
    //var textnode = document.createTextNode("Address:"+a[i]+"<br>"+"RoomNumber"+b[i]);
    //sort by occupancy 
        var textnode=document.createElement("div")//set textnode to input imformation
        textnode.innerText=WorstOccuBucket[prop][index].address+";"+"Rm "+WorstOccuBucket[prop][index].roomNumber
        var textnode1=document.createElement("div")
        textnode1.innerText="Occupancy: "+WorstOccuBucket[prop][index].Occupancy 
        var textnode2=document.createElement("div")               
        var ValueTransformed=BooleanToOnOff(WorstOccuBucket,prop,index)
        textnode2.innerText="Heating/cooling: "+ValueTransformed[0]
        var textnode3=document.createElement("div")
        textnode3.innerText="Lights:"+ValueTransformed[1]
        var textnode4=document.createElement("div")
        textnode4.innerText=WorstOccuBucket[prop][index].timeChecked
        td.appendChild(textnode)
        td.appendChild(textnode1)
        td.appendChild(textnode2)
        td.appendChild(textnode3)
        td.appendChild(textnode4)
        tr.appendChild(td);
        tbody.appendChild(tr)
        table.appendChild(tbody)
    }
  //set textnode as the combination of roomnumber and address
        node.appendChild(table)//if table is tr,then the last tr is appended to node
}


function BooleanToOnOff(WorstOccuBucket,prop,index)
{
    var heatingCoolingOn="Off"
    var lightsOn="Off"
    var ValueTransformed=[heatingCoolingOn,lightsOn]
    if(WorstOccuBucket[prop][index].heatingCoolingOn==true)
        {
           ValueTransformed[0]="On"
        }
    if(WorstOccuBucket[prop][index]==true)
        {
           ValueTransformed[1]="On"
        }
    return ValueTransformed
}