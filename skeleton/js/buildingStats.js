"use strict";

//F13: Building with wasteful observations
var roomusagelist=loadlistfromlocalstorage(key)
var Bukcet=roomusagelist.aggregateBy()
var buildingBucket=Bukcet["buildingBucket"]
var BuildingStatistics=roomusagelist.Statistics(buildingBucket)
function BuildingStatsHTML(BuildingStatistics)
{
    if(localStorage.getItem(key)!=undefined)
        {
            var Counter=0
            for(var prop in BuildingStatistics)
                {
                 document.getElementById("content").innerHTML +=//generate an id to that will passes into Wasteful func as a parameter for each th element
                 `<div class="mdl-cell mdl-cell--4-col">
                      <table class="observation-table mdl-data-table mdl-js-data-table mdl-shadow--2dp">
                          <thead>
                                <tr><th class="mdl-data-table__cell--non-numeric" id=${Counter}>  
                                      <h4>
                                          ${prop}
                                      </h4>
                                </th></tr>
                          </thead>
                          <tbody>
                                <tr><td class="mdl-data-table__cell--non-numeric">
                                        Observations: ${BuildingStatistics[prop].ObservationsTotal}<br />
                                        Wasteful observations: ${BuildingStatistics[prop].WastefulObservations}<br />
                                        Average seat utilisation: ${BuildingStatistics[prop].SeatsUtilization}<br />
                                        Average lights utilisation: ${BuildingStatistics[prop].lightsUtilization}<br />
                                        Average heating/cooling utilisation: ${BuildingStatistics[prop].heatingCoolingUtiliztion}
                                </td></tr>
                          </tbody>
                      </table>
                  </div>`
                  WastefulMarker(prop,BuildingStatistics,Counter)
                  Counter++//Counter should be put below WastefulMraker function to call this function at this iteration of this counter
                }
        }else
        {
            displayMessage("LocalStorage of the key is undefined",10000)
        }
}


function WastefulMarker(prop,BuildingStatistics,Counter)
{
    if(BuildingStatistics[prop].WastefulObservations>0)
        {
          var table=document.getElementsByTagName("TABLE")[Counter]
          var CounterString=String(Counter)
          var h4ElementForColorChange=document.getElementById(CounterString)
          h4ElementForColorChange.style.backgroundColor='red'
        }
}
window.onload=BuildingStatsHTML(BuildingStatistics)